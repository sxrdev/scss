#bat
Width BAT file you can run and watch the SASS/SCSS files
Content of BAT file:

```
cd "PATH"
sass --watch [path/name.scss]:[path/name.css] --style=compressed 
```